import requests
import json
from datetime import datetime, timedelta
import os
import sys

def get_ods_data(base_url, token, output_path, from_date=None, to_date=None):
    # Endpoint URL
    url = f"{base_url}/ods_data"

    # Headers
    headers = {
        "X-Api-Token": token,
        "X-Api-Elaborate": "true",
        "Content-Type": "application/json"
    }

    # Query parameters
    params = {
        "page": 1,
        "items_per_page": 100,  # Adjust as needed
        "sort": "DateTimeCreated",
        "sort_asc": "false",
    }

    # Add date range parameters if provided
    if from_date:
        params['from'] = from_date
    if to_date:
        params['to'] = to_date

    # If no date range is provided, set 'from' to the start of the current day
    if not from_date and not to_date:
        params['from'] = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0).strftime('%Y-%m-%d %H:%M:%S')

    try:
        # Make the GET request
        response = requests.get(url, headers=headers, params=params)
        response.raise_for_status()  # Raise an exception for bad status codes

        # Parse the JSON response
        data = response.json()

        # Get the directory path
        dir_path = os.path.dirname(output_path)

        # Create the output directory if it doesn't exist
        if dir_path and not os.path.exists(dir_path):
            os.makedirs(dir_path)

        # Save the data to a JSON file
        with open(output_path, 'w') as f:
            json.dump(data, f, indent=2)

        print(f"Data successfully saved to {output_path}")

    except requests.RequestException as e:
        print(f"An error occurred while making the request: {e}")
    except json.JSONDecodeError:
        print("Failed to decode the JSON response")
    except IOError as e:
        print(f"An error occurred while writing to the file: {e}")
    except Exception as e:
        print(f"An unexpected error occurred: {e}")

if __name__ == "__main__":
    # Configuration
    BASE_URL = "http://localhost:3000/zms/ra"  
    TOKEN = "rpu_Ik89eH2peCdNlImcVx9vKawgoxzoAWQvcYf6YjJ1KwQHXyoTqe" 
    
    # Use the current directory if no argument is provided
    output_dir = sys.argv[1] if len(sys.argv) > 1 else "."
    
    # Create filename with timestamp
    filename = f"ods_data_{datetime.now().strftime('%Y%m%d_%H%M%S')}.json"
    
    # Combine directory and filename
    OUTPUT_PATH = os.path.join(output_dir, filename)

    # Optional: Provide date range
    # Format should be 'YYYY-MM-DD HH:MM:SS'
    FROM_DATE = None  # e.g., '2023-10-04 21:00:00'
    TO_DATE = None    # e.g., '2023-10-04 23:00:00'

    get_ods_data(BASE_URL, TOKEN, OUTPUT_PATH, FROM_DATE, TO_DATE)