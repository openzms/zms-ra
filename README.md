<!--
SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
SPDX-FileCopyrightText: 2024-present University of Colorado Boulder
SPDX-License-Identifier: Apache-2.0
-->

# zms-ra (OpenZMS Radio Astronomy service)

The [OpenZMS software](https://gitlab.flux.utah.edu/openzms) is a prototype
automatic spectrum-sharing management system for radio dynamic zones.
OpenZMS provides mechanisms to share electromagnetic (radio-frequency)
spectrum between experimental or test systems and existing spectrum users,
and between multiple experimental systems.

This component was developed as a collaboration between the University of Utah (OpenZMS) and the University of Colorado Boulder (WIRG group).

This `zms-ra` repository contains a Radio Astronomy (RA) service that serves as the backend for the RA component of OpenZMS. It has been contributed to OpenZMS (University of Utah) by the WIRG group from CU Boulder. The service enables Radio Astronomy Observatories to create RA observations in OpenZMS using the TARDyS4 format. It also serves the `/ods_data` endpoint for external access to the RA observation schedule. Initial use of the `/ods_data` endpoint is in development by SpaceX for spectrum sharing with Starlink constellation satellites during RA observations.

## Quick dev mode start

Build the project:

```
make
```

The [zms-identity](https://gitlab.flux.utah.edu/openzms/zms-identity) service
verifies tokens and establishes RBAC for the RA service. Therefore, you
must start that service prior to running this service, and must point this
service to the identity service's southbound (gRPC) endpoint at start.

Run the `identity` service as instructed in the
[zms-identity instructions](https://gitlab.flux.utah.edu/openzms/zms-identity).

If you ran the `identity` service with default endpoints, you shouldn't need
to specify any endpoint arguments for the `ra` service. If you changed the
`identity` service's endpoints, you will need to customize the arguments
below to correspond (see `zms-ra --help`).

Run the `zms-ra` service:

```
mkdir -p tmp
./build/output/zms-ra 
    --db-dsn tmp/zms-ra.sqlite 
    -debug
```
## Accessing the RA Observation Schedule

The RA observation schedule can be accessed via the `/ods_data` endpoint. This endpoint is designed for external use, particularly for spectrum sharing purposes. For example, SpaceX is developing integration with this endpoint to enable spectrum sharing between Starlink constellation satellites and Radio Astronomy observations.

For more information on using the `/ods_data` endpoint or integrating with the RA service, please refer to the API documentation or contact the OpenZMS team.