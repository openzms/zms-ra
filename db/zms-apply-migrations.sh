#!/bin/sh

#
# This script is intended to run as part of an OpenZMS service's
# entrypoint.sh, prior to service startup.  If automatic migrations are
# enabled, and they fail, the service must not start.
#
# The script can use the following env vars:
#
# DB_DRIVER=(postgres|sqlite)
# DB_AUTO_MIGRATE=(0|1)
# DB_AUTO_MIGRATE_URL="postgres://postgres:postgres@${DBHOST}/${DBNAME}?search_path=public&sslmode=disable"
#   (or, for sqlite)
# DB_AUTO_MIGRATE_URL="sqlite:///data/zms-identity.sqlite"
#

#
# Nearly identical to https://pkg.go.dev/strconv#ParseBool .
#
# 1, t, T, TRUE, true, True evaluate to 0 (shell true); anything else
# evaluates to 1 (shell false).
#
parseBool() {
    val="${1}"
    if [ -z "$val" ]; then
        false
    elif [ "$val" = "1" -o "$val" = "t" -o "$val" = "T" \
         -o "$val" = "TRUE" -o "$val" = "true" -o "$val" = "True" ]; then
        true
    else
        false
    fi
}

if [ -z "$DB_DRIVER" ]; then
    echo "Error: must set DB_DRIVER=(postgres|sqlite)"
    exit 1
elif [ ! \( "$DB_DRIVER" = "postgres" -o "$DB_DRIVER" = "sqlite" \) ]; then
    echo "Error: unsupported ${DB_DRIVER}: must be postgres or sqlite"
    exit 1
fi

MIGDIR="file://migrations/${DB_DRIVER}?format=golang-migrate"

parseBool "$DB_AUTO_MIGRATE"
DB_AUTO_MIGRATE=$?
parseBool "$DB_SKIP_MIGRATE_CHECK"
DB_SKIP_MIGRATE_CHECK=$?

if [ $DB_SKIP_MIGRATE_CHECK -eq 0 ]; then
    echo "Migration status check disabled; skipping checks and migrations!"
    exit 0
else
    echo "Checking migration status..."
fi

atlas migrate status --dir "${MIGDIR}" --url "${DB_AUTO_MIGRATE_URL}"

STATUS=`atlas migrate status --dir "${MIGDIR}" --url "${DB_AUTO_MIGRATE_URL}"  --format '{{.Status}}'`
if [ ! $? -eq 0 ]; then
    echo "Failed to run 'atlas migrate status'... aborting"
    exit 1
fi
echo "$STATUS" | grep -q "^OK"
STATUS_OK=$?
echo "$STATUS" | grep -q "^PENDING"
STATUS_PENDING=$?
if [ $STATUS_OK -eq 0 ]; then
    echo "Database migrations up to date, nothing to apply."
    exit 0
elif [ $STATUS_PENDING -eq 0 ]; then
    echo "Database has pending migrations."
else
    echo "Error: unexpected database migration state '$STATUS'; aborting"
    exit 1
fi

PENDING=`atlas migrate status --dir "${MIGDIR}" --url "${DB_AUTO_MIGRATE_URL}" --format '{{ len .Pending }}'`
if [ ! $? -eq 0 ]; then
    echo "Failed to run 'atlas migrate status' (pending); aborting."
    exit 1
fi

echo "Atlas migrations: status=$STATUS, pending=$PENDING"

if [ -z "$PENDING" -o $PENDING -eq 0 ]; then
    echo "No database migrations to apply."
    exit 0
fi

if [ $DB_AUTO_MIGRATE -eq 1 ]; then
    echo "Auto-migrations disabled; not applying migrations; aborting."
    exit 1
fi

atlas migrate apply --dir "${MIGDIR}" --url "${DB_AUTO_MIGRATE_URL}" $PENDING
if [ ! $? -eq 0 ]; then
    echo "Failed to apply migrations!"
    exit 111
fi
