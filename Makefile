# SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
#
# SPDX-License-Identifier: Apache-2.0

.PHONY: build
all: build

buildinfo:
	if [ `git log --pretty=format:%h -n 1` ]; then \
		COMMIT=`git log --pretty=format:%h -n 1` ; \
		DIRTY=`git diff --quiet --exit-code || echo -dirty` ; \
		TAG=`git describe --exact-match --tags` ; \
		BRANCH=`git rev-parse --abbrev-ref HEAD` ; \
	fi
	echo "package version" > pkg/version/buildinfo.go
	echo >> pkg/version/buildinfo.go
	echo 'var _commit string = "'${COMMIT}${DIRTY}'"' >> pkg/version/buildinfo.go
	echo 'var _branch string = "'${BRANCH}'"' >> pkg/version/buildinfo.go
	echo 'var _tag string = "'${TAG}'"' >> pkg/version/buildinfo.go
	echo 'var _buildTimestamp string = ""' >> pkg/version/buildinfo.go
	echo >> pkg/version/buildinfo.go

build: buildinfo
	go build -o ./build/output/zms-ra ./cmd/zms-ra

image:
	docker build -f deploy/Dockerfile -t gitlab.flux.utah.edu:4567/openzms/zms-ra/zms-ra:latest .

clean:
	rm -rf ./build/output ./cmd/zms-ra/zms-ra