// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0
package config

import (
	"flag"
	"os"
	"strconv"
)

type Config struct {
	Verbose              bool
	Debug                bool
	HttpEndpointListen   string
	HttpEndpoint         string
	HealthHttpEndpoint   string
	RpcEndpointListen    string
	RpcEndpoint          string
	ServiceId            string
	ServiceName          string
	DbDriver             string
	DbDsn                string
	DbGormMigrate        bool
	Bootstrap            bool
	IdentityRpcEndpoint  string
	FrontendHttpEndpoint string
	DbAutoMigrate        bool
}

func getEnvBool(name string, defValue bool) bool {
	if val, ok := os.LookupEnv(name); ok {
		if pval, err := strconv.ParseBool(val); err == nil {
			return pval
		}
	}
	return defValue
}

func getEnvString(name string, defValue string) string {
	if val, ok := os.LookupEnv(name); ok {
		return val
	}
	return defValue
}

func LoadConfig() *Config {
	var config Config

	flag.BoolVar(&config.Debug, "debug",
		getEnvBool("LOG_DEBUG", false), "Enable debug logging.")
	flag.BoolVar(&config.Verbose, "verbose",
		getEnvBool("LOG_VERBOSE", false), "Enable verbose logging.")
	flag.StringVar(&config.HttpEndpointListen, "http-endpoint-listen",
		getEnvString("HTTP_ENDPOINT_LISTEN", "0.0.0.0:8050"), "HTTP endpoint bind address")
	flag.StringVar(&config.HttpEndpoint, "http-endpoint",
		getEnvString("HTTP_ENDPOINT", "0.0.0.0:8050"), "HTTP endpoint address")
	flag.StringVar(&config.HealthHttpEndpoint, "health-http-endpoint",
		getEnvString("HEALTH_HTTP_ENDPOINT", "0.0.0.0:8051"), "HTTP health endpoint address")
	flag.StringVar(&config.RpcEndpointListen, "rpc-endpoint-listen",
		getEnvString("RPC_ENDPOINT_LISTEN", "0.0.0.0:8052"), "RPC endpoint bind address")
	flag.StringVar(&config.RpcEndpoint, "rpc-endpoint",
		getEnvString("RPC_ENDPOINT", "0.0.0.0:8052"), "RPC endpoint address")
	flag.StringVar(&config.ServiceId, "service-id",
		getEnvString("SERVICE_ID", "33453677-4573-4b4a-b006-8fd073220003"), "Service ID")
	flag.StringVar(&config.ServiceName, "service-name",
		getEnvString("SERVICE_NAME", "zms-ra"), "Service Name")
	flag.StringVar(&config.DbDriver, "db-driver",
		getEnvString("DB_DRIVER", "sqlite"), "Database driver (sqlite, postgres)")
	flag.StringVar(&config.DbDsn, "db-dsn",
		getEnvString("DB_DSN", "file::memory:?cache=shared"), "Database DSN")
	flag.BoolVar(&config.DbGormMigrate, "db-gorm-migrate",
		getEnvBool("DB_GORM_MIGRATE", false), "Enable/disable GORM automatic database migration. WARNING: you should never do this. Use Atlas migrations!")
	flag.BoolVar(&config.Bootstrap, "bootstrap",
		getEnvBool("BOOTSTRAP", false), "Enable/disable database bootstrap")
	flag.StringVar(&config.IdentityRpcEndpoint, "identity-rpc-endpoint",
		getEnvString("IDENTITY_RPC_ENDPOINT", "0.0.0.0:8002"), "Identity service RPC endpoint address")
	flag.StringVar(&config.FrontendHttpEndpoint, "frontend-http-endpoint",
		getEnvString("FRONTEND_HTTP_ENDPOINT", "0.0.0.0:8000"), "Frontend service HTTP endpoint address")
	flag.BoolVar(&config.DbAutoMigrate, "db-auto-migrate",
		getEnvBool("DB_AUTO_MIGRATE", false), "Enable/disable automatic database migration")

	flag.Parse()
	return &config
}
