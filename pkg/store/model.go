// SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package store

import (
	"errors"
	"fmt"
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

type RAObservation struct {
	Id                  uuid.UUID  `gorm:"type:uuid;primary_key;column:id;not null"`
	TransactionId       *string    `gorm:"column:transaction_id"`
	GrantId             uuid.UUID  `gorm:"column:grant_id;not null"`
	DateTimePublished   time.Time  `gorm:"column:date_time_published;not null"`
	DateTimeCreated     time.Time  `gorm:"column:date_time_created;not null"`
	Checksum            *string    `gorm:"column:checksum;not null"`
	DpaId               *string    `gorm:"column:dpa_id;not null"`
	DpaName             *string    `gorm:"column:dpa_name;not null"`
	LocLat              *float64   `gorm:"column:loc_lat;not null"`
	LocLong             *float64   `gorm:"column:loc_long;not null"`
	LocRadius           *float64   `gorm:"column:loc_radius;not null"`
	LocElevation        *float64   `gorm:"column:loc_elevation;not null"`
	LocComment          *string    `gorm:"column:loc_comment"`
	CoordType           *string    `gorm:"column:coord_type;not null"`
	NumberEvents        int64      `gorm:"column:number_events"`
	EventId             int64      `gorm:"column:event_id"`
	EventComment        *string    `gorm:"column:event_comment"`
	EventStatus         *string    `gorm:"column:event_status"`
	DateTimeStart       time.Time  `gorm:"column:date_time_start;not null"`
	DateTimeStop        time.Time  `gorm:"column:date_time_stop;not null"`
	Acquisition         float64    `gorm:"column:acquisition"`
	FreqStart           float64    `gorm:"column:freq_start;not null"`
	FreqStop            float64    `gorm:"column:freq_stop;not null"`
	SourceId            *string    `gorm:"column:source_id"`
	ObsType             *string    `gorm:"column:obs_type"`
	CorrInt             float64    `gorm:"column:corr_int"`
	RegionSize          float64    `gorm:"column:region_size"`
	RegionX             float64    `gorm:"column:region_x"`
	RegionY             float64    `gorm:"column:region_y"`
	TrkRateDecDegPerSec *float64   `gorm:"column:trk_rate_dec_deg_per_sec"`
	TrkRateRaDegPerSec  *float64   `gorm:"column:trk_rate_ra_deg_per_sec"`
	Enabled             bool       `gorm:"column:enabled"`
	CreatorId           *string    `gorm:"column:creator_id;not null"`
	UpdaterId           *string    `gorm:"column:updater_id"`
	CreatedAt           *time.Time `gorm:"column:created_at;not null"`
	UpdatedAt           *time.Time `gorm:"column:updated_at"`
	DeletedAt           *time.Time `gorm:"column:deleted_at"`
}

func (x *RAObservation) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

var AllTables []interface{} = []interface{}{
	&RAObservation{},
}

type Comparator string

const (
	CompNotEqual           Comparator = "ne"
	CompEqual              Comparator = "eq"
	CompGreaterThan        Comparator = "gt"
	CompLessThan           Comparator = "lt"
	CompGreaterThanOrEqual Comparator = "gte"
	CompLessThanOrEqual    Comparator = "lte"
)

// NB: ensure this matches the above enumerated string type.
const comparatorCreatePostgres string = "CREATE TYPE comparator_enum AS ENUM ('ne', 'eq', 'gt', 'lt', 'gte', 'lte')"

func (c *Comparator) Scan(value interface{}) error {
	s, ok := value.(string)
	if !ok {
		return errors.New("invalid Comparator value")
	}
	*c = Comparator(s)
	return nil
}

func (c Comparator) Value() (interface{}, error) {
	return string(c), nil
}

func (Comparator) GormDBDataType(db *gorm.DB, field *schema.Field) string {
	switch db.Dialector.Name() {
	case "sqlite":
		return "text"
	case "postgres":
		return "comparator_enum"
	}
	return ""
}

func CreateComparatorEnumPostgres(db *gorm.DB) (err error) {
	res := db.Debug().Exec(fmt.Sprintf(`
	DO $$ BEGIN
		%s;
	EXCEPTION
		WHEN duplicate_object THEN null;
	END $$;`, comparatorCreatePostgres))
	if res.Error != nil {
		return res.Error
	}
	return nil
}
