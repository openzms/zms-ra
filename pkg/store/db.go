// SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package store

import (
	"errors"
	"fmt"
	"time"

	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"gitlab.flux.utah.edu/openzms/zms-ra/pkg/config"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func GetDatabase(serverConfig *config.Config, dbConfig *gorm.Config) (db *gorm.DB, err error) {
	var localConfig gorm.Config
	if dbConfig != nil {
		localConfig = *dbConfig
	}
	localConfig.NowFunc = func() time.Time { return time.Now().UTC() }
	switch serverConfig.DbDriver {
	case "sqlite":
		db, err = gorm.Open(sqlite.Open(serverConfig.DbDsn), &localConfig)
	case "postgres":
		db, err = gorm.Open(postgres.Open(serverConfig.DbDsn), &localConfig)
	default:
		db, err = nil, errors.New("unsupported database driver")
	}
	return db, err
}

func InitDatabase(serverConfig *config.Config, db *gorm.DB) (err error) {
	if serverConfig.DbAutoMigrate {
		err = MigrateDatabase(serverConfig, db)
	}
	return err
}

func MigrateDatabase(serverConfig *config.Config, db *gorm.DB) error {
	if db.Dialector.Name() == "postgres" {
		if err := CreateComparatorEnumPostgres(db); err != nil {
			return err
		}
	}
	return db.AutoMigrate(
		AllTables...,
	)
}

func PostRegisterBootstrap(serverConfig *config.Config, db *gorm.DB, bootstrapElementId *uuid.UUID, bootstrapUserId *uuid.UUID) error {
	if bootstrapElementId != nil && bootstrapUserId != nil {
		log.Debug().Msg(fmt.Sprintf("bootstrap info: %+v, %+v", bootstrapElementId, bootstrapUserId))
	}
	return nil
}

func Paginate(page int, itemsPerPage int) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		offset := (page - 1) * itemsPerPage
		return db.Offset(offset).Limit(itemsPerPage)
	}
}

func MakeILikeClause(config *config.Config, field string) string {
	if config.DbDriver == "postgres" {
		return field + " ilike ?"
	} else {
		return field + " like ?"
	}
}
