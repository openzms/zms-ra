// SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package northbound

import (
	"context"
	"errors"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"gorm.io/gorm"

	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/client"
	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/subscription"

	"gitlab.flux.utah.edu/openzms/zms-ra/pkg/config"
	"gitlab.flux.utah.edu/openzms/zms-ra/pkg/store"
)

type Server struct {
	gin           *gin.Engine
	server        *http.Server
	config        *config.Config
	db            *gorm.DB
	sm            *subscription.SubscriptionManager[*subscription.Event]
	rclient       *client.ZmsClient
	running       bool
	ginHealth     *gin.Engine
	serverHealth  *http.Server
	serviceSecret string
}

func NewServer(serverConfig *config.Config, db *gorm.DB, sm *subscription.SubscriptionManager[*subscription.Event], rclient *client.ZmsClient) (server *Server, err error) {
	err = nil
	server = &Server{
		config:  serverConfig,
		db:      db,
		sm:      sm,
		rclient: rclient,
	}
	if !serverConfig.Debug {
		gin.SetMode(gin.ReleaseMode)
	}
	server.gin = gin.Default()
	server.server = &http.Server{
		Addr:    serverConfig.HttpEndpointListen,
		Handler: server.gin,
	}
	server.SetupRoutes()

	server.ginHealth = gin.Default()
	server.serverHealth = &http.Server{
		Addr:    serverConfig.HealthHttpEndpoint,
		Handler: server.ginHealth,
	}
	server.SetupHealthRoutes()

	bootstrapElementId, bootstrapUserId := server.rclient.GetBootstrapInfo()
	if err = store.PostRegisterBootstrap(serverConfig, db, bootstrapElementId, bootstrapUserId); err != nil {
		return nil, err
	}

	server.running = false

	return server, nil
}

func (s *Server) Run() (err error) {
	if s.running {
		return errors.New("server already running")
	}
	go func() {
		log.Debug().Msg("starting http health server")
		if err = s.serverHealth.ListenAndServe(); err != nil {
			if err == http.ErrServerClosed {
				log.Info().Err(err).Msg("http health listener stopped")
			} else {
				log.Error().Err(err).Msg("http health listener error")
			}
		}
	}()
	go func() {
		log.Debug().Msg("starting http server")
		if err = s.server.ListenAndServe(); err != nil {
			if err == http.ErrServerClosed {
				log.Info().Err(err).Msg("http listener stopped")
			} else {
				log.Error().Err(err).Msg("http listener error")
			}
		}
	}()
	s.running = true
	return err
}

func (s *Server) Shutdown(ctx context.Context) (err error) {
	if !s.running {
		return errors.New("server not running")
	}

	shutdownCtx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()
	if err := s.server.Shutdown(shutdownCtx); err != nil {
		log.Error().Err(err).Msg("failed to stop http server")
	}
	if errHealth := s.serverHealth.Shutdown(shutdownCtx); err != nil {
		log.Error().Err(errHealth).Msg("failed to stop http health server")
		s.running = false
		return errHealth
	} else {
		s.running = false
		return nil
	}
	select {
	case <-shutdownCtx.Done():
		if err = ctx.Err(); err != nil {
			log.Error().Err(err).Msg("failed to stop http server")
		} else {
			log.Info().Msg("gracefully stopped http server")
		}
		s.running = false
		return err
	}
	return nil
}
