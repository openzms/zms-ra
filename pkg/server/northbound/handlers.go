// SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package northbound

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"math"
	"net/http"
	"sort"

	// "regexp"
	"bytes"
	"io"
	"strconv"
	"strings"
	"time"

	"github.com/fatih/structs"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"github.com/rs/zerolog/log"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"

	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/client"
	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/policy"
	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/subscription"
	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/token"

	identity "gitlab.flux.utah.edu/openzms/zms-api/go/zms/identity/v1"

	"gitlab.flux.utah.edu/openzms/zms-ra/pkg/store"
	"gitlab.flux.utah.edu/openzms/zms-ra/pkg/version"
)

func (s *Server) SetupHealthRoutes() error {
	s.ginHealth.GET("/health/alive", s.GetHealthAlive)
	s.ginHealth.GET("/health/ready", s.GetHealthReady)
	return nil
}

func (s *Server) GetHealthAlive(c *gin.Context) {
	c.Status(http.StatusOK)
}

func (s *Server) GetHealthReady(c *gin.Context) {
	if db, err := s.db.DB(); err == nil {
		if err := db.Ping(); err == nil {
			c.Status(http.StatusOK)
			return
		}
	}
	c.Status(http.StatusServiceUnavailable)
}

// When assigning a UUID to a *string field
func assignUUID(id uuid.UUID) *string {
	idStr := id.String()
	return &idStr
}

// When reading a *string field as a UUID
func readUUID(idPtr *string) (uuid.UUID, error) {
	if idPtr == nil {
		return uuid.Nil, errors.New("ID is nil")
	}
	return uuid.Parse(*idPtr)
}

func CheckElaborateMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		if tokenString := c.GetHeader("X-Api-Elaborate"); tokenString == "true" {
			c.Set("zms.elaborate", true)
		} else {
			c.Set("zms.elaborate", false)
		}
	}
}

func CheckForceUpdateMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		if tokenString := c.GetHeader("X-Api-Force-Update"); tokenString == "true" {
			c.Set("zms.force-update", true)
		} else {
			c.Set("zms.force-update", false)
		}
	}
}

func CheckTokenValue(s *Server, tokenString string) (*client.CachedToken, int, error) {
	if err := token.Validate(tokenString); err != nil {
		return nil, http.StatusBadRequest, fmt.Errorf("invalid token")
	}

	if tok, err := s.rclient.LookupToken(tokenString); tok != nil && err == nil {
		return tok, http.StatusOK, nil
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	var identityClient identity.IdentityClient
	var terr error
	if identityClient, terr = s.rclient.GetIdentityClient(ctx); terr != nil || identityClient == nil {
		return nil, http.StatusBadRequest, fmt.Errorf("identity service unavailable")
	}

	ctx, cancel = context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	tt := true
	req := identity.GetTokenRequest{
		Header: &identity.RequestHeader{
			Elaborate: &tt,
		},
		Token: tokenString,
	}
	resp, err := identityClient.GetToken(ctx, &req)
	if err != nil {
		rpcErr, _ := status.FromError(err)
		if rpcErr.Code() == codes.NotFound {
			return nil, http.StatusNotFound, fmt.Errorf("token not found")
		} else if rpcErr.Code() == codes.Unauthenticated {
			return nil, http.StatusUnauthorized, fmt.Errorf(rpcErr.Message())
		} else {
			return nil, http.StatusInternalServerError, fmt.Errorf("identity service error: %v", rpcErr.Message())
		}
	}
	if tok, err := s.rclient.CheckAndCacheToken(resp.Token); err != nil {
		return nil, http.StatusForbidden, fmt.Errorf("invalid token: %s", err.Error())
	} else {
		return tok, http.StatusOK, nil
	}
}

func CheckTokenMiddleware(s *Server) gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error
		tokenString := c.GetHeader("X-Api-Token")
		if tokenString == "" {
			c.Set("zms.token", nil)
			return
		}
		var tok *client.CachedToken
		var code int
		if tok, code, err = CheckTokenValue(s, tokenString); err != nil {
			c.AbortWithStatusJSON(code, gin.H{"error": err.Error()})
			return
		} else {
			c.Set("zms.token", tok)
		}
	}
}

func (s *Server) CheckPolicyMiddleware(t *client.CachedToken, targetUserId *uuid.UUID, targetElementId *uuid.UUID, policies []policy.Policy) (bool, string) {
	log.Debug().Msg(fmt.Sprintf("token: %+v", t))
	for _, p := range policies {
		log.Debug().Msg(fmt.Sprintf("policy: %s", p.Name))
		for _, trb := range t.RoleBindings {
			if match := p.Check(trb.Role.Value, &t.UserId, trb.ElementId, targetUserId, targetElementId); match == true {
				log.Debug().Msg(fmt.Sprintf("policy match: %s", p.Name))
				return true, p.Name
			} else {
				log.Debug().Msg(fmt.Sprintf("policy mismatch: %s", p.Name))
			}
		}
	}
	return false, ""
}

func (s *Server) RequireContextToken(c *gin.Context) (*client.CachedToken, error) {
	value, exists := c.Get("zms.token")
	if !exists || value == nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "missing X-Api-Token"})
		return nil, errors.New("missing X-Api-Token")
	}
	return value.(*client.CachedToken), nil
}

func CorsMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Next()
	}
}

func (s *Server) SetupRoutes() error {
	v1 := s.gin.Group("/v1")

	v1.Use(CorsMiddleware())
	v1.Use(CheckElaborateMiddleware())
	v1.Use(CheckForceUpdateMiddleware())
	v1.Use(CheckTokenMiddleware(s))

	v1.GET("/version", s.GetVersion)

	v1.GET("/raobservations", s.GetRAObservationsList)
	v1.POST("/raobservations", s.CreateRAObservation)
	v1.PUT("/raobservations/:raobservation_id", s.UpdateRAObservation)
	v1.GET("/raobservations/:raobservation_id", s.GetRAObservation)
	v1.DELETE("/raobservations/:raobservation_id", s.DeleteRAObservation)

	v1.GET("/subscriptions", s.GetSubscriptions)
	v1.POST("/subscriptions", s.CreateSubscription)
	v1.DELETE("/subscriptions/:subscription_id", s.DeleteSubscription)
	v1.GET("/subscriptions/:subscription_id/events", s.GetSubscriptionEvents)

	/* Endpoint to serve in ODS data format */
	v1.GET("/ods_data", s.GetOdsData)

	return nil
}

func GetPaginateParams(c *gin.Context) (int, int) {
	page, _ := strconv.Atoi(c.Query("page"))
	if page < 1 {
		page = 1
	}
	itemsPerPage, _ := strconv.Atoi(c.Query("items_per_page"))
	if itemsPerPage < 10 {
		itemsPerPage = 10
	} else if itemsPerPage > 100 {
		itemsPerPage = 100
	}
	return page, itemsPerPage
}

func Paginate(page int, itemsPerPage int) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		offset := (page - 1) * itemsPerPage
		return db.Offset(offset).Limit(itemsPerPage)
	}
}

func (s *Server) GetVersion(c *gin.Context) {
	if _, err := s.RequireContextToken(c); err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}
	c.JSON(http.StatusOK, version.GetVersion())
}

type GetRAObservationsListQueryParams struct {
	Observation *string `form:"observation" binding:"omitempty"`
	Current     *bool   `form:"current" binding:"omitempty"`
	Future      *bool   `form:"future" binding:"omitempty"`
	Past        *bool   `form:"past" binding:"omitempty"`
	Sort        *string `form:"sort" binding:"omitempty,oneof=DateTimePublished DateTimeCreated CreatedAt UpdatedAt"`
	SortAsc     *bool   `form:"sort_asc" binding:"omitempty"`
}

func generateChecksum(item interface{}) (string, error) {
	itemMap, ok := item.(map[string]interface{})
	if !ok {
		return "", fmt.Errorf("item is not a map")
	}

	copyMap := make(map[string]interface{})
	for k, v := range itemMap {
		if k != "TransactionId" && k != "Checksum" {
			copyMap[k] = v
		}
	}

	// Sort the keys
	keys := make([]string, 0, len(copyMap))
	for k := range copyMap {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	var sum int
	for _, k := range keys {
		v := copyMap[k]
		stringValue := fmt.Sprintf("%v", v)
		log.Debug().Msgf("Key: %s, Value: %s", k, stringValue)
		for _, ch := range stringValue {
			sum += int(ch)
		}
	}

	checksum := fmt.Sprintf("%08x", sum%0xFFFFFFFF)
	result := strings.ToLower(checksum[len(checksum)-8:])
	log.Debug().Msgf("Calculated checksum: %s", result)
	return result, nil
}

func ValidateAndFillRAObservation(raObservation *store.RAObservation, creatorId uuid.UUID) error {
	if raObservation == nil {
		return errors.New("RAObservation cannot be nil")
	}

	now := time.Now()

	// Fill in fields
	if raObservation.Id == uuid.Nil {
		raObservation.Id = uuid.New()
	}

	if raObservation.TransactionId == nil || *raObservation.TransactionId == "" {
		newTransactionId := uuid.New().String()
		raObservation.TransactionId = &newTransactionId
	}

	if raObservation.DateTimePublished.IsZero() {
		raObservation.DateTimePublished = now
	}

	if raObservation.DateTimeCreated.IsZero() {
		raObservation.DateTimeCreated = now
	}

	// Validate TransactionId
	if raObservation.TransactionId == nil || *raObservation.TransactionId == "" {
		return errors.New("invalid TransactionId")
	}

	// Validate DateTime fields
	if raObservation.DateTimePublished.After(now) {
		return errors.New("invalid DateTimePublished")
	}
	if raObservation.DateTimeCreated.After(now) {
		return errors.New("invalid DateTimeCreated")
	}
	if raObservation.DateTimeStart.IsZero() {
		return errors.New("DateTimeStart is required")
	}
	if raObservation.DateTimeStop.IsZero() {
		return errors.New("DateTimeStop is required")
	}
	if raObservation.DateTimeStop.Before(raObservation.DateTimeStart) {
		return errors.New("DateTimeStop must be after DateTimeStart")
	}

	// Log the RAObservation before checksum calculation
	log.Debug().Msgf("RAObservation before checksum calculation: %+v", raObservation)

	calculatedChecksum, err := generateChecksum(structs.Map(raObservation))
	if err != nil {
		return fmt.Errorf("failed to generate checksum: %v", err)
	}

	log.Debug().Msgf("Calculated checksum: %s, Received checksum: %s", calculatedChecksum, *raObservation.Checksum)

	// Validate DPA or Location
	if raObservation.DpaId == nil && raObservation.DpaName == nil {
		if raObservation.LocLat == nil || raObservation.LocLong == nil {
			return errors.New("either DPA information or location information is required")
		}
		// Validate latitude and longitude
		if *raObservation.LocLat < -90 || *raObservation.LocLat > 90 {
			return errors.New("invalid LocLat value")
		}
		if *raObservation.LocLong < -180 || *raObservation.LocLong > 180 {
			return errors.New("invalid LocLong value")
		}
	}

	// Validate LocRadius
	if raObservation.LocRadius != nil && *raObservation.LocRadius < 0 {
		return errors.New("LocRadius must be non-negative")
	}

	// Validate CoordType
	if raObservation.CoordType == nil || (*raObservation.CoordType != "azel" && *raObservation.CoordType != "radec") {
		return errors.New("CoordType must be either 'azel' or 'radec'")
	}

	// Validate NumberEvents
	if raObservation.NumberEvents <= 0 {
		return errors.New("NumberEvents must be positive")
	}

	// Validate EventId
	if raObservation.EventId <= 0 {
		return errors.New("EventId must be positive")
	}

	// Validate EventStatus
	validStatuses := map[string]bool{"actual": true, "projected": true}
	if raObservation.EventStatus == nil || !validStatuses[*raObservation.EventStatus] {
		return errors.New("invalid EventStatus")
	}

	// Validate Acquisition
	if raObservation.Acquisition <= 0 {
		return errors.New("acquisition must be positive")
	}

	// Validate Frequency
	if raObservation.FreqStart <= 0 {
		return errors.New("FreqStart must be positive")
	}
	if raObservation.FreqStop <= 0 {
		return errors.New("FreqStop must be positive")
	}
	if raObservation.FreqStop <= raObservation.FreqStart {
		return errors.New("FreqStop must be greater than FreqStart")
	}

	// Validate ObsType
	validObsTypes := map[string]bool{"pulsar": true, "continuum": true, "spectral": true} // Add more as needed
	if raObservation.ObsType == nil || !validObsTypes[*raObservation.ObsType] {
		return errors.New("invalid ObsType")
	}

	// Validate CorrInt
	if raObservation.CorrInt <= 0 {
		return errors.New("CorrInt must be positive")
	}

	// Validate RegionSize
	if raObservation.RegionSize <= 0 {
		return errors.New("RegionSize must be positive")
	}

	// Validate RegionX and RegionY based on CoordType
	if *raObservation.CoordType == "azel" {
		if raObservation.RegionX < 0 || raObservation.RegionX > 360 {
			return errors.New("invalid RegionX for azimuth")
		}
		if raObservation.RegionY < -90 || raObservation.RegionY > 90 {
			return errors.New("invalid RegionY for elevation")
		}
	} else { // radec
		if raObservation.RegionX < 0 || raObservation.RegionX >= 24 {
			return errors.New("invalid RegionX for right ascension")
		}
		if raObservation.RegionY < -90 || raObservation.RegionY > 90 {
			return errors.New("invalid RegionY for declination")
		}
	}

	// Fill in other fields
	raObservation.Enabled = true
	creatorIdStr := creatorId.String()
	raObservation.CreatorId = &creatorIdStr
	if raObservation.CreatedAt == nil {
		raObservation.CreatedAt = &now
	}
	raObservation.UpdatedAt = &now

	return nil
}

func printTableSchema(db *gorm.DB, tableName string) {
	var columns []struct {
		ColumnName string
		DataType   string
		IsNullable string
	}

	result := db.Raw(`
        SELECT column_name, data_type, is_nullable
        FROM information_schema.columns
        WHERE table_name = ?
        ORDER BY ordinal_position
    `, tableName).Scan(&columns)

	if result.Error != nil {
		fmt.Printf("Error fetching schema for table %s: %v\n", tableName, result.Error)
		return
	}

	fmt.Printf("Schema for table %s:\n", tableName)
	fmt.Println(strings.Repeat("-", 60))
	fmt.Printf("%-20s %-15s %-10s\n", "Column Name", "Data Type", "Nullable")
	fmt.Println(strings.Repeat("-", 60))

	for _, col := range columns {
		fmt.Printf("%-20s %-15s %-10s\n", col.ColumnName, col.DataType, col.IsNullable)
	}
	fmt.Println(strings.Repeat("-", 60))
}

func PrintFirstFiveRAObservations(db *gorm.DB) {
	var observations []struct {
		ID                uuid.UUID `gorm:"column:id"`
		TransactionId     *string   `gorm:"column:transaction_id"`
		DateTimePublished time.Time `gorm:"column:date_time_published"`
		DateTimeCreated   time.Time `gorm:"column:date_time_created"`
		DpaId             *string   `gorm:"column:dpa_id"`
		DpaName           *string   `gorm:"column:dpa_name"`
		FreqStart         float64   `gorm:"column:freq_start"`
		FreqStop          float64   `gorm:"column:freq_stop"`
		DateTimeStart     time.Time `gorm:"column:date_time_start"`
		DateTimeStop      time.Time `gorm:"column:date_time_stop"`
	}

	result := db.Table("ra_observations").Limit(5).Find(&observations)
	if result.Error != nil {
		fmt.Printf("Error fetching RA Observations: %v\n", result.Error)
		return
	}

	fmt.Println("First 5 RA Observations:")
	fmt.Println(strings.Repeat("-", 100))
	for i, obs := range observations {
		fmt.Printf("Observation %d:\n", i+1)
		fmt.Printf("  ID: %s\n", obs.ID)
		fmt.Printf("  Transaction ID: %s\n", stringPtrValue(obs.TransactionId))
		fmt.Printf("  Published: %s\n", obs.DateTimePublished.Format(time.RFC3339))
		fmt.Printf("  Created: %s\n", obs.DateTimeCreated.Format(time.RFC3339))
		fmt.Printf("  DPA ID: %s\n", stringPtrValue(obs.DpaId))
		fmt.Printf("  DPA Name: %s\n", stringPtrValue(obs.DpaName))
		fmt.Printf("  Freq Start: %.2f MHz\n", obs.FreqStart/1e6)
		fmt.Printf("  Freq Stop: %.2f MHz\n", obs.FreqStop/1e6)
		fmt.Printf("  Start Time: %s\n", obs.DateTimeStart.Format(time.RFC3339))
		fmt.Printf("  Stop Time: %s\n", obs.DateTimeStop.Format(time.RFC3339))
		fmt.Println(strings.Repeat("-", 100))
	}
}

// Helper function to safely print string pointer values
func stringPtrValue(s *string) string {
	if s == nil {
		return "nil"
	}
	return *s
}

func (s *Server) GetOdsData(c *gin.Context) {
	// Ensure caller provided a valid token
	tok, err := s.RequireContextToken(c)
	if err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	// Check policy
	policies := policy.MakeMatchAdminOrElementRolePolicy(policy.RoleViewer, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, nil, nil, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Parse query parameters
	fromStr := c.Query("from")
	toStr := c.Query("to")

	var fromTime, toTime time.Time
	var queryErr error

	now := time.Now()
	startOfDay := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())
	endOfDay := startOfDay.Add(24 * time.Hour)

	if fromStr != "" {
		fromTime, queryErr = time.Parse("2006-01-02 15:04:05", fromStr)
		if queryErr != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid 'from' date format. Use 'YYYY-MM-DD HH:MM:SS'"})
			return
		}
	} else {
		fromTime = startOfDay
	}

	if toStr != "" {
		toTime, queryErr = time.Parse("2006-01-02 15:04:05", toStr)
		if queryErr != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid 'to' date format. Use 'YYYY-MM-DD HH:MM:SS'"})
			return
		}
	} else {
		toTime = endOfDay
	}

	// Build the query
	query := s.db.Model(&store.RAObservation{}).
		Where("deleted_at IS NULL").
		Where("(date_time_start BETWEEN ? AND ?) OR (date_time_stop BETWEEN ? AND ?) OR (date_time_start <= ? AND date_time_stop >= ?)",
			fromTime, toTime, fromTime, toTime, fromTime, toTime)

	var raObservations []store.RAObservation
	result := query.Find(&raObservations)
	if result.Error != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to retrieve RA Observations"})
		return
	}

	odsData := make([]map[string]interface{}, 0)

	for _, obs := range raObservations {
		odsItem := map[string]interface{}{
			"site_id":       obs.DpaName,
			"site_lat_deg":  obs.LocLat,
			"site_lon_deg":  obs.LocLong,
			"site_el_m":     obs.LocElevation,
			"src_id":        obs.SourceId,
			"src_start_utc": obs.DateTimeStart.Format(time.RFC3339Nano),
			"src_end_utc":   obs.DateTimeStop.Format(time.RFC3339Nano),
			"freq_lower_hz": obs.FreqStart,
			"freq_upper_hz": obs.FreqStop,
			"notes":         obs.EventComment,
		}

		// Handle optional fields
		if obs.ObsType != nil && *obs.ObsType == "pulsar" {
			odsItem["src_is_pulsar_bool"] = true
		} else {
			odsItem["src_is_pulsar_bool"] = false
			odsItem["corr_integ_time_sec"] = obs.CorrInt
		}

		if obs.CoordType != nil && *obs.CoordType == "radec" {
			odsItem["src_ra_j2000_deg"] = obs.RegionX
			odsItem["src_dec_j2000_deg"] = obs.RegionY
		}

		odsItem["src_radius"] = obs.RegionSize
		odsItem["slew_sec"] = obs.Acquisition

		if obs.TrkRateDecDegPerSec != nil {
			odsItem["trk_rate_dec_deg_per_sec"] = *obs.TrkRateDecDegPerSec
		}
		if obs.TrkRateRaDegPerSec != nil {
			odsItem["trk_rate_ra_deg_per_sec"] = *obs.TrkRateRaDegPerSec
		}

		odsData = append(odsData, odsItem)
	}

	c.JSON(http.StatusOK, gin.H{"ods_data": odsData})
}

func (s *Server) GetRAObservationsList(c *gin.Context) {
	var err error

	// Check optional filter query parameters
	params := GetRAObservationsListQueryParams{}
	if err = c.ShouldBindQuery(&params); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	page, itemsPerPage := GetPaginateParams(c)

	// Ensure caller provided a valid token
	var tok *client.CachedToken
	if tok, err = s.RequireContextToken(c); err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	// Check policy
	policies := policy.MakeMatchAdminOrElementRolePolicy(policy.RoleViewer, policy.GreaterOrEqual)
	var match bool
	if match, _ = s.CheckPolicyMiddleware(tok, nil, nil, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Print the schema for the ra_observations table (Debugging)
	// var schemaPrinted bool
	// if !schemaPrinted {
	// 	printTableSchema(s.db, "ra_observations")
	// 	schemaPrinted = true
	// }

	// PrintFirstFiveRAObservations(s.db)

	var raObservationList []store.RAObservation
	q := s.db.Unscoped().Where("deleted_at IS NULL")

	if params.Observation != nil && *params.Observation != "" {
		q = q.Where("site_id LIKE ? OR src_id LIKE ?", "%"+*params.Observation+"%", "%"+*params.Observation+"%")
	}

	now := time.Now()
	if params.Current != nil && *params.Current {
		q = q.Where("date_time_start <= ? AND date_time_stop >= ?", now, now)
	}
	if params.Future != nil && *params.Future {
		q = q.Where("date_time_start > ?", now)
	}
	if params.Past != nil && *params.Past {
		q = q.Where("date_time_stop < ?", now)
	}

	sortDir := "desc"
	if params.SortAsc != nil && *params.SortAsc {
		sortDir = "asc"
	}

	if params.Sort != nil {
		// Map the frontend sort keys to the correct database column names
		sortColumnMap := map[string]string{
			"DateTimeCreated":   "date_time_created",
			"DateTimePublished": "date_time_published",
			"CreatedAt":         "created_at",
			"UpdatedAt":         "updated_at",
		}

		if dbColumn, exists := sortColumnMap[*params.Sort]; exists {
			q = q.Order(dbColumn + " " + sortDir)
		} else {
			q = q.Order("date_time_created " + sortDir) // Default sort
		}
	} else {
		q = q.Order("date_time_created " + sortDir) // Default sort
	}

	var total int64
	cres := q.Model(&store.RAObservation{}).Count(&total)
	if cres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": cres.Error.Error()})
		return
	}

	qres := q.Scopes(Paginate(page, itemsPerPage)).Find(&raObservationList)
	if qres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": qres.Error.Error()})
		return
	}

	// Convert the RAObservation structs to match the frontend expectations
	var formattedObservations []map[string]interface{}
	for _, obs := range raObservationList {
		formattedObs := map[string]interface{}{
			"TransactionId":     obs.TransactionId,
			"GrantId":           obs.GrantId,
			"DateTimePublished": obs.DateTimePublished,
			"DateTimeCreated":   obs.DateTimeCreated,
			"DpaId":             obs.DpaId,
			"DpaName":           obs.DpaName,
			"LocLat":            obs.LocLat,
			"LocLong":           obs.LocLong,
			"LocRadius":         obs.LocRadius,
			"LocElevation":      obs.LocElevation,
			"LocComment":        obs.LocComment,
			"CoordType":         obs.CoordType,
			"NumberEvents":      obs.NumberEvents,
			"EventId":           obs.EventId,
			"EventComment":      obs.EventComment,
			"EventStatus":       obs.EventStatus,
			"DateTimeStart":     obs.DateTimeStart,
			"DateTimeStop":      obs.DateTimeStop,
			"Acquisition":       obs.Acquisition,
			"FreqStart":         obs.FreqStart,
			"FreqStop":          obs.FreqStop,
			"SourceId":          obs.SourceId,
			"ObsType":           obs.ObsType,
			"CorrInt":           obs.CorrInt,
			"RegionSize":        obs.RegionSize,
			"RegionX":           obs.RegionX,
			"RegionY":           obs.RegionY,
			"Enabled":           obs.Enabled,
			"CreatorId":         obs.CreatorId,
			"Checksum":          obs.Checksum,
		}
		formattedObservations = append(formattedObservations, formattedObs)
	}

	c.JSON(http.StatusOK, gin.H{
		"ra_observations": formattedObservations,
		"page":            page,
		"total":           total,
		"pages":           int(math.Ceil(float64(total) / float64(itemsPerPage))),
	})
}

func (s *Server) CreateRAObservation(c *gin.Context) {
	var err error

	log.Debug().Msg("Starting CreateRAObservation function")
	// Log the raw request body
	body, err := io.ReadAll(c.Request.Body)
	if err != nil {
		log.Error().Err(err).Msg("Failed to read request body")
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to read request body"})
		return
	}
	c.Request.Body = io.NopCloser(bytes.NewBuffer(body))
	log.Debug().Msgf("Raw request body: %s", string(body))
	// Ensure caller provided a valid token
	var tok *client.CachedToken
	if tok, err = s.RequireContextToken(c); err != nil {
		log.Error().Err(err).Msg("Failed to get context token")
		return
	}
	log.Debug().
		Str("userId", tok.UserId.String()).
		Str("tokenId", tok.Id.String()).
		Int("roleBindingsCount", len(tok.RoleBindings)).
		Msg("Token validated")

	// Check policy
	policies := policy.MakeMatchAdminOrElementRolePolicy(policy.RoleOperator, policy.GreaterOrEqual)
	var match bool
	var matchedPolicy string
	if match, matchedPolicy = s.CheckPolicyMiddleware(tok, nil, nil, policies); !match {
		log.Warn().
			Str("userId", tok.UserId.String()).
			Msg("Policy check failed: user not authorized")
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}
	log.Debug().
		Str("userId", tok.UserId.String()).
		Str("matchedPolicy", matchedPolicy).
		Msg("Policy check passed")

	var raObservation store.RAObservation
	if err := c.ShouldBindJSON(&raObservation); err != nil {
		log.Error().Err(err).Msg("Failed to bind JSON to RAObservation")
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	log.Debug().
		Interface("raObservation", raObservation).
		Msg("Successfully bound JSON to RAObservation")

	// Validate and fill the observation
	if err := ValidateAndFillRAObservation(&raObservation, tok.UserId); err != nil {
		log.Error().Err(err).Msg("Failed to validate and fill RAObservation")
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	log.Debug().
		Str("raObservationId", raObservation.Id.String()).
		Time("dateTimeCreated", raObservation.DateTimeCreated).
		Msg("Successfully validated and filled RAObservation")

	// Check for time overlap and source ID overlap
	var overlappingObservations []store.RAObservation
	err = s.db.Where("(date_time_start < ? AND date_time_stop > ?) OR (date_time_start < ? AND date_time_stop > ?)",
		raObservation.DateTimeStop, raObservation.DateTimeStart,
		raObservation.DateTimeStop, raObservation.DateTimeStart).
		Find(&overlappingObservations).Error

	if err != nil {
		log.Error().Err(err).Msg("Failed to query for overlapping observations")
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to check for overlapping observations"})
		return
	}

	for _, existingObs := range overlappingObservations {
		if *existingObs.SourceId == *raObservation.SourceId {
			log.Warn().
				Str("existingObsId", existingObs.Id.String()).
				Str("newObsId", raObservation.Id.String()).
				Str("sourceId", *raObservation.SourceId).
				Msg("Overlapping observation with same source ID detected")
			c.JSON(http.StatusConflict, gin.H{"error": "An observation with the same source ID already exists for the specified time range"})
			return
		}
	}

	if err := s.db.Create(&raObservation).Error; err != nil {
		log.Error().Err(err).Msg("Failed to create RAObservation in database")
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create RAObservation"})
		return
	}
	log.Info().
		Str("raObservationId", raObservation.Id.String()).
		Str("creatorId", *raObservation.CreatorId).
		Msg("Successfully created RAObservation in database")

	c.JSON(http.StatusCreated, raObservation)
	log.Debug().Msg("CreateRAObservation function completed successfully")
}

func (s *Server) UpdateRAObservation(c *gin.Context) {
	// Check required path parameters
	transactionId := c.Param("raobservation_id")
	if transactionId == "" {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "missing transaction ID"})
		return
	}

	// Ensure caller provided a valid token
	tok, err := s.RequireContextToken(c)
	if err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	// Check policy
	policies := policy.MakeMatchAdminOrElementRolePolicy(policy.RoleOperator, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, nil, nil, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	var existingObservation store.RAObservation
	if err := s.db.Unscoped().Where("transaction_id = ? AND deleted_at IS NULL", transactionId).First(&existingObservation).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "RAObservation not found"})
		} else {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": "Failed to retrieve RAObservation"})
		}
		return
	}

	var updatedObservation store.RAObservation
	if err := c.ShouldBindJSON(&updatedObservation); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Ensure the TransactionId matches
	if updatedObservation.TransactionId == nil || *updatedObservation.TransactionId != transactionId {
		c.JSON(http.StatusBadRequest, gin.H{"error": "TransactionId mismatch"})
		return
	}

	// Validate the updated observation
	if err := ValidateAndFillRAObservation(&updatedObservation, tok.UserId); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Check for time overlap and source ID overlap
	var overlappingObservations []store.RAObservation
	err = s.db.Where("transaction_id != ? AND ((date_time_start < ? AND date_time_stop > ?) OR (date_time_start < ? AND date_time_stop > ?))",
		transactionId, updatedObservation.DateTimeStop, updatedObservation.DateTimeStart,
		updatedObservation.DateTimeStop, updatedObservation.DateTimeStart).
		Find(&overlappingObservations).Error

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to check for overlapping observations"})
		return
	}

	for _, existingObs := range overlappingObservations {
		if *existingObs.SourceId == *updatedObservation.SourceId {
			c.JSON(http.StatusConflict, gin.H{"error": "An observation with the same source ID already exists for the specified time range"})
			return
		}
	}

	// Update the existing observation with the new data
	existingObservation = updatedObservation
	existingObservation.UpdaterId = assignUUID(tok.UserId)
	now := time.Now()
	existingObservation.UpdatedAt = &now

	if err := s.db.Save(&existingObservation).Error; err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update RAObservation"})
		return
	}

	c.JSON(http.StatusOK, existingObservation)
}

func (s *Server) GetRAObservation(c *gin.Context) {
	var err error

	// Check required path parameters
	transactionID := c.Param("raobservation_id")
	if transactionID == "" {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "missing transaction ID"})
		return
	}

	// Ensure caller provided a valid token
	var tok *client.CachedToken
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Check policy
	policies := policy.MakeMatchAdminOrElementRolePolicy(policy.RoleViewer, policy.GreaterOrEqual)
	var match bool
	if match, _ = s.CheckPolicyMiddleware(tok, nil, nil, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	var raObservation store.RAObservation
	if err := s.db.Unscoped().Where("transaction_id = ? AND deleted_at IS NULL", transactionID).First(&raObservation).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "RA Observation not found"})
		} else {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": "Failed to retrieve RA Observation"})
		}
		return
	}

	c.JSON(http.StatusOK, raObservation)
}

func (s *Server) DeleteRAObservation(c *gin.Context) {
	// Check required path parameters
	transactionId := c.Param("raobservation_id")
	if transactionId == "" {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "missing transaction ID"})
		return
	}

	// Ensure caller provided a valid token
	tok, err := s.RequireContextToken(c)
	if err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	// Check policy
	policies := policy.MakeMatchAdminOrElementRolePolicy(policy.RoleAdmin, policy.Equal)
	match, _ := s.CheckPolicyMiddleware(tok, nil, nil, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	var raObservation store.RAObservation
	if err := s.db.Unscoped().Where("transaction_id = ? AND deleted_at IS NULL", transactionId).First(&raObservation).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "RAObservation not found"})
		} else {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": "Failed to retrieve RAObservation"})
		}
		return
	}

	// Perform soft delete
	if err := s.db.Model(&raObservation).Update("deleted_at", time.Now()).Error; err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete RAObservation"})
		return
	}

	c.Status(http.StatusNoContent)
}

type CreateSubscriptionModel struct {
	Id      string                     `json:"id" binding:"required,uuid"`
	Filters []subscription.EventFilter `json:"filters" binding:"omitempty"`
}

func (s *Server) CreateSubscription(c *gin.Context) {
	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Initial input validation.
	var dm CreateSubscriptionModel
	if err := c.ShouldBindJSON(&dm); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// NB: for now, non-admins will be restricted to self-user-associated
	// events.
	if match, _ := s.CheckPolicyMiddleware(tok, nil, nil, policy.AdminOnlyPolicy); !match {

		// Build a map of elementId string to (max) role value in that
		// element.
		// Also build a list of all ElementIds in the token.
		// Also build a list of ElementIds with role >= Operator.
		elementRoleMap := make(map[string]int)
		elementIds := make([]string, 0, len(tok.RoleBindings))
		opElementIds := make([]string, 0)
		for _, rb := range tok.RoleBindings {
			if rb.ElementId == nil || rb.Role == nil {
				continue
			}
			eis := rb.ElementId.String()
			nv := rb.Role.Value
			if v, ok := elementRoleMap[eis]; ok {
				if nv > v {
					elementRoleMap[eis] = nv
				}
			} else {
				elementRoleMap[eis] = nv
			}
			elementIds = append(elementIds, eis)
			if nv >= policy.RoleOperator {
				opElementIds = append(opElementIds, eis)
			}
		}
		userIds := []string{tok.UserId.String()}

		//
		// Each filter must match these constraint checks:
		//   * have UserIds set to exactly the calling user, and must
		//     have ElementIds set to a subset of the ElementIds in the token; OR
		//   * have UserIds set to exactly the calling user, and ElementIds nil; OR
		//   * have UserIds set to nil, and ElementIds set to elements in which the user has >= Operator role.
		//
		if dm.Filters == nil || len(dm.Filters) < 1 {
			if false {
				c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "non-admin user must provide at least one filter, with UserIds set to exactly their user ID, and ElementIds to a subset of the calling token's element IDs."})
				return
			} else {
				dm.Filters = []subscription.EventFilter{
					subscription.EventFilter{UserIds: userIds},
					subscription.EventFilter{UserIds: userIds, ElementIds: elementIds},
				}
				if len(opElementIds) > 0 {
					dm.Filters = append(dm.Filters, subscription.EventFilter{ElementIds: opElementIds})
				}
				log.Debug().Msg(fmt.Sprintf("autocreated user subscription %+v for token %+v", dm, tok))
			}
		}

		// Filters must be bound to calling user unless user has >= operator
		// role in the associated elements.
		for _, filter := range dm.Filters {
			if filter.UserIds == nil || len(filter.UserIds) == 0 {
				// In this case, the user must provide at least one element
				// in which they have >= Operator role.
				if len(opElementIds) == 0 {
					msg := "non-admin, non-operator token subscriptions must set UserIds to exactly their token UserId"
					c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": msg})
					return
				} else if filter.ElementIds == nil || len(filter.ElementIds) == 0 {
					msg := "token subscriptions without UserIds must set ElementIds to at least one Element in which they have >= Operator role."
					c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": msg})
					return
				} else {
					for _, eis := range filter.ElementIds {
						if rv, ok := elementRoleMap[eis]; !ok || rv < policy.RoleOperator {
							msg := fmt.Sprintf("non-admin, wildcard user filter cannot include a sub-Operator role (element %s)", eis)
							c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": msg})
							return
						}
					}
				}
			} else {
				if len(filter.UserIds) != 1 || filter.UserIds[0] != tok.UserId.String() {
					msg := "non-admin subscription must set each filter.UserIds field to a one-item list containing exactly the calling token UserId."
					c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": msg})
					return
				}
				if filter.ElementIds == nil || len(filter.ElementIds) < 1 {
					//msg := "non-admin subscription must set each filter.ElementIds field to a subset of the ElementIds in the calling token."
					//c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": msg})
					//return
				} else {
					for _, elementId := range filter.ElementIds {
						if _, ok := elementRoleMap[elementId]; !ok {
							msg := "non-admin user must set each filter's ElementIds field to a subset of the ElementIds in the calling token."
							c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": msg})
							return
						}
					}
				}
			}
		}
	}

	if err := s.sm.Subscribe(dm.Id, dm.Filters, nil, c.ClientIP(), subscription.Rest, &tok.Token); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.Status(http.StatusCreated)

	return
}

func (s *Server) GetSubscriptions(c *gin.Context) {
	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Check policy.
	if match, _ := s.CheckPolicyMiddleware(tok, nil, nil, policy.AdminOnlyPolicy); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"subscriptions": s.sm.GetSubscriptions()})
	return
}

func (s *Server) GetSubscriptionEvents(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("subscription_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
	} else {
		id = idParsed
	}
	idStr := id.String()

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	var subprotos []string
	if xApiTok := c.GetHeader("X-Api-Token"); xApiTok != "" {
		if tok, err = s.RequireContextToken(c); err != nil {
			return
		}
	} else if wsProto := c.GetHeader("Sec-WebSocket-Protocol"); wsProto != "" {
		if wsTok, code, altErr := CheckTokenValue(s, wsProto); altErr != nil {
			log.Warn().Msg(fmt.Sprintf("bad token (%s) in Sec-WebSocket-Protocol: %+v", wsProto, altErr.Error()))
			c.AbortWithStatusJSON(code, gin.H{"error": altErr.Error()})
			return
		} else {
			tok = wsTok
			subprotos = append(subprotos, wsProto)
		}
	}
	if tok == nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "no token provided"})
		return
	} else {
		log.Debug().Msg(fmt.Sprintf("valid token from websocket"))
	}

	ech := make(chan *subscription.Event)
	if err := s.sm.UpdateChannel(idStr, ech, &tok.Token); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// See if client wants to reuse subscription upon websocket close.
	deleteOnClose := true
	if tokenString := c.GetHeader("X-Api-Delete-On-Close"); tokenString == "false" {
		c.Set("zms.deleteOnClose", false)
		deleteOnClose = false
	} else {
		c.Set("zms.deleteOnClose", true)
	}

	defer func() {
		if deleteOnClose {
			s.sm.Unsubscribe(idStr)
		} else {
			log.Debug().Msg(fmt.Sprintf("retaining subscription %s", idStr))
			s.sm.UpdateChannel(idStr, nil, &tok.Token)
		}
	}()

	wsUpgrader := websocket.Upgrader{
		ReadBufferSize:  0,
		WriteBufferSize: 16384,
		Subprotocols:    subprotos,
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}

	var conn *websocket.Conn
	if conn, err = wsUpgrader.Upgrade(c.Writer, c.Request, nil); err != nil {
		log.Debug().Msg(fmt.Sprintf("error upgrading websocket conn: %+v", err.Error()))
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	// Handle client disconnect.
	cch := make(chan interface{})
	go func() {
		for {
			_, _, err := conn.ReadMessage()
			if err != nil {
				cch <- nil
				conn.Close()
				return
			}
		}
	}()

	// Read events from SubscriptionManager, and client read errors or
	// client disconnection.
	for {
		select {
		case e := <-ech:
			if body, jErr := json.Marshal(e); jErr != nil {
				log.Error().Err(jErr).Msg(fmt.Sprintf("error marshaling event (%+v): %s", e, err.Error()))
				continue
			} else {
				if err = conn.WriteMessage(websocket.TextMessage, body); err != nil {
					return
				}
			}
		case <-cch:
			// NB: the deferred handler covers this case.
			return
		}
	}

	return
}

func (s *Server) DeleteSubscription(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("subscription_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
	} else {
		id = idParsed
	}
	idStr := id.String()

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	var sub *subscription.Subscription[*subscription.Event]
	if sub = s.sm.GetSubscription(idStr); sub == nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "not found"})
		return
	}

	// Check policy.
	if match, _ := s.CheckPolicyMiddleware(tok, nil, nil, policy.AdminOnlyPolicy); !match {
		if sub.Token == nil || *sub.Token != tok.Token {
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
			return
		}
	}

	if err := s.sm.Unsubscribe(idStr); err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "not found"})
		return
	}

	c.Status(http.StatusAccepted)

	return
}
