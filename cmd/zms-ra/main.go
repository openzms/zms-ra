// SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"

	identity "gitlab.flux.utah.edu/openzms/zms-api/go/zms/identity/v1"
	zmsclient "gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/client"
	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/subscription"

	"gitlab.flux.utah.edu/openzms/zms-ra/pkg/config"
	"gitlab.flux.utah.edu/openzms/zms-ra/pkg/server/northbound"
	"gitlab.flux.utah.edu/openzms/zms-ra/pkg/store"
	"gitlab.flux.utah.edu/openzms/zms-ra/pkg/version"
)

func main() {
	serverConfig := config.LoadConfig()
	fmt.Printf("serverConfig: %+v\n", serverConfig)

	if serverConfig.Debug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	} else if serverConfig.Verbose {
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	} else {
		zerolog.SetGlobalLevel(zerolog.WarnLevel)
	}

	dbConfig := gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	}

	db, err := store.GetDatabase(serverConfig, &dbConfig)
	if err != nil {
		log.Fatal().Err(err).Msg("failed to connect to database")
	}

	err = store.InitDatabase(serverConfig, db)
	if err != nil {
		log.Fatal().Err(err).Msg("failed to initialize database")
	}

	sm := subscription.NewSubscriptionManager[*subscription.Event]()

	clientConfig := zmsclient.ZmsClientConfig{
		IdentityRpcEndpoint:   serverConfig.IdentityRpcEndpoint,
		WatchIdentityServices: true,
		WatchIdentityTokens:   true,
	}
	rclient := zmsclient.New(clientConfig)

	if serverConfig.IdentityRpcEndpoint != "" {
		service := &identity.Service{
			Id:             serverConfig.ServiceId,
			Name:           serverConfig.ServiceName,
			Kind:           "ra",
			Endpoint:       serverConfig.HttpEndpoint,
			EndpointApiUri: serverConfig.HttpEndpoint,
			Description:    "OpenZMS RA (Radio Astronomy) service",
			Version:        version.VersionString,
			ApiVersion:     "v1",
			Enabled:        true,
		}

		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()
		if err = rclient.RegisterService(ctx, service); err != nil {
			log.Fatal().Err(err).Msg(fmt.Sprintf("failed to register service (%+v) at identity service (%s)", service, serverConfig.IdentityRpcEndpoint))
		}
	}

	httpServer, err := northbound.NewServer(serverConfig, db, sm, &rclient)
	if err != nil {
		log.Error().Err(err).Msg("failed to start http server; terminating")
		os.Exit(1)
	}
	httpServer.Run()

	// Run until signaled
	sigch := make(chan os.Signal, 1)
	signal.Notify(sigch, syscall.SIGINT, syscall.SIGTERM)
	sig := <-sigch
	log.Info().Msg(fmt.Sprintf("shutting down server (signal %d)", sig))

	sm.Shutdown()

	if err := httpServer.Shutdown(context.Background()); err != nil {
		log.Error().Err(err).Msg("failed to gracefully stop http server; terminating")
	}

	log.Info().Msg("server shutdown complete")
}
