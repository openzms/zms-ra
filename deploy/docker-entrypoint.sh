#!/bin/sh

( cd /usr/local/share/zms-ra/db \
      && /usr/local/share/zms-ra/db/zms-apply-migrations.sh ) \
   || exit $?

exec /usr/local/bin/zms-ra
