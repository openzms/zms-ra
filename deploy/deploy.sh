#!/bin/bash

# Define the names of the containers you want to manage
POSTGRES_CONTAINER_NAME="zms-ra-postgres-local-dev"
RA_CONTAINER_NAME="zms-ra-local-dev"

# Function to remove a container if it exists
remove_container() {
    local CONTAINER_NAME=$1
    
    # Check if the container exists
    if [ "$(docker ps -aq -f name=$CONTAINER_NAME)" ]; then
        echo "Container $CONTAINER_NAME already exists. Removing it..."
        
        # Stop the container if it's running
        if [ "$(docker ps -q -f name=$CONTAINER_NAME)" ]; then
            docker stop $CONTAINER_NAME
        fi
        
        # Remove the container
        docker rm $CONTAINER_NAME
        echo "Container $CONTAINER_NAME removed."
    else
        echo "Container $CONTAINER_NAME does not exist."
    fi
}

# Remove the existing containers
remove_container $POSTGRES_CONTAINER_NAME
remove_container $RA_CONTAINER_NAME

# Bring up the services using docker-compose
echo "Starting up containers..."
docker-compose up -d $POSTGRES_CONTAINER_NAME
sleep 4
docker-compose up -d $RA_CONTAINER_NAME 

if [ $? -ne 0 ]; then
    echo "Failed to start one or more containers. Please check the logs above."
    exit 1
fi

# Create two panes to observe the logs of both containers
echo "Opening two panes to observe the logs..."

# Check if tmux is installed
if ! command -v tmux &> /dev/null
then
    echo "tmux could not be found, please install it to use the pane feature."
    exit 1
fi

# Start a new tmux session with two panes
tmux new-session -d -s container_logs

# Split the window into two panes
tmux split-window -h

# Run the logs in each pane
tmux send-keys -t container_logs:0.0 "docker-compose logs -f $POSTGRES_CONTAINER_NAME" C-m
tmux send-keys -t container_logs:0.1 "docker-compose logs -f $RA_CONTAINER_NAME" C-m

# Attach to the tmux session
tmux attach-session -t container_logs
